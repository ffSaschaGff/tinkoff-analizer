package repo

func (instrumentType InstrumentType) ToDB() string {
	switch instrumentType {
	case InstrumentTypeShare:
		return "shares"
	case InstrumentTypeBond:
		return "bond"
	case InstrumentTypeEtf:
		return "etf"
	case InstrumentTypeCurrency:
		return "currency"
	}

	return ""
}

func InstrumentTypeFromDB(in string) InstrumentType {
	switch in {
	case "shares":
		return InstrumentTypeShare
	case "bond":
		return InstrumentTypeBond
	case "etf":
		return InstrumentTypeEtf
	case "currency":
		return InstrumentTypeCurrency
	}

	return InstrumentTypeUndefined
}

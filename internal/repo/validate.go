package repo

func (analytics FullAnalyticsType) Validate() error {
	if analytics.Name == "" {
		return ErrInvalidData
	}

	for _, option := range analytics.Options {
		if option.Name == "" {
			return ErrInvalidData
		}
	}

	return nil
}

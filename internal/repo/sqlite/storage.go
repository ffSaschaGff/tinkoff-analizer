package sqlite

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3" // sqlite driver
	"github.com/pressly/goose"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/common/optional"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/config"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type storage struct {
	database *sqlx.DB
}

type instrumentDBO struct {
	ID             string
	Tiker          string
	Name           string
	Currency       string
	Lot            int32
	InstrumentType string
}

// NewMarketRepo - PostgresMarketRepo constructor.
func NewStorage() (repo.Storage, error) {
	database, err := sqlx.Connect("sqlite3", string(config.GetValue(config.SQLiteConn)))
	if err != nil {
		return nil, err
	}
	database.SetMaxOpenConns(config.GetValue(config.DatabaseMaxConns).Int())
	database.SetConnMaxIdleTime(time.Minute * time.Duration(config.GetValue(config.DatabaseConnLifeTimeMin).Int64()))
	database.SetConnMaxLifetime(time.Minute * time.Duration(config.GetValue(config.DatabaseConnLifeTimeMin).Int64()))

	if err := goose.SetDialect("sqlite3"); err != nil {
		return nil, err
	}

	if err := goose.Up(database.DB, "migrations/sqlite"); err != nil {
		return nil, err
	}

	return &storage{
		database: database,
	}, nil
}

func (s *storage) GetFullInstrument(ctx context.Context, id string) (repo.FullInstrument, error) {
	qb := squirrel.Select(
		"tiker",
		"name",
		"currency",
		"type",
		"lot",
	).
		From("instrument").
		Where(squirrel.Eq{"id": id}).
		Limit(1).
		PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return repo.FullInstrument{}, err
	}
	row := s.database.QueryRowContext(ctx, query, args...)
	instrument := instrumentDBO{}
	err = row.Scan(
		&instrument.Tiker,
		&instrument.Name,
		&instrument.Currency,
		&instrument.InstrumentType,
		&instrument.Lot,
	)
	if err != nil {
		return repo.FullInstrument{}, err
	}

	qb = squirrel.Select(
		"at.id as type_id",
		"av.analytic_id as value_id",
	).
		From("analytic_types as at").
		InnerJoin("analytic_value as av on av.analytic_types = at.id").
		Where(squirrel.Eq{"av.instrument_id": id}).
		PlaceholderFormat(squirrel.Dollar)

	query, args, err = qb.ToSql()
	if err != nil {
		return repo.FullInstrument{}, err
	}
	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return repo.FullInstrument{}, err
	}
	defer closeRows(rows)

	analytics := map[int64]int64{}
	var (
		typeID  int64
		valueID int64
	)
	for rows.Next() {
		err = rows.Scan(
			&typeID,
			&valueID,
		)
		if err != nil {
			return repo.FullInstrument{}, err
		}
		analytics[typeID] = valueID
	}

	return repo.FullInstrument{
		Instrument: repo.Instrument{
			ID:             id,
			Tiker:          optional.From(instrument.Tiker),
			Name:           optional.From(instrument.Name),
			Currency:       optional.From(instrument.Currency),
			Lot:            optional.From(instrument.Lot),
			InstrumentType: optional.From(repo.InstrumentTypeFromDB(instrument.InstrumentType)),
		},
		Analytics: analytics,
	}, nil
}

func (s *storage) UpdateFullInstrument(ctx context.Context, instrument repo.FullInstrument) error {
	tx, err := s.database.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer rollbackTX(tx)

	deleteBuilder := squirrel.Delete("analytic_value").
		Where(squirrel.Eq{"instrument_id": instrument.Instrument.ID}).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err := deleteBuilder.ToSql()
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	insertBuilder := squirrel.Insert("analytic_value").Columns(
		"analytic_types",
		"instrument_id",
		"analytic_id",
	).PlaceholderFormat(squirrel.Dollar)
	for analyticType, analyticValue := range instrument.Analytics {
		insertBuilder = insertBuilder.Values(
			analyticType,
			instrument.Instrument.ID,
			analyticValue,
		)
	}
	query, args, err = insertBuilder.ToSql()
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	valuesToSet := map[string]interface{}{}
	if instrument.Instrument.Tiker.Valid {
		valuesToSet["tiker"] = instrument.Instrument.Tiker.Value
	}
	if instrument.Instrument.Tiker.Valid {
		valuesToSet["name"] = instrument.Instrument.Name.Value
	}
	if instrument.Instrument.Tiker.Valid {
		valuesToSet["currency"] = instrument.Instrument.Currency.Value
	}
	if instrument.Instrument.Tiker.Valid {
		valuesToSet["type"] = instrument.Instrument.InstrumentType.Value.ToDB()
	}
	if instrument.Instrument.Tiker.Valid {
		valuesToSet["lot"] = instrument.Instrument.Lot.Value
	}

	if len(valuesToSet) > 0 {
		updateBuilder := squirrel.Update("instrument").SetMap(
			valuesToSet,
		).Where(squirrel.Eq{"id": instrument.Instrument.ID}).PlaceholderFormat(squirrel.Dollar)
		query, args, err = updateBuilder.ToSql()
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}

func (s *storage) ListFullAnalyticsTypes(ctx context.Context) ([]repo.FullAnalyticsType, error) {
	options, err := s.getAllOptions(ctx)
	if err != nil {
		return nil, err
	}

	qb := squirrel.Select(
		"id",
		"name",
	).From("analytic_types").PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	var (
		result []repo.FullAnalyticsType
		dbo    repo.FullAnalyticsType
	)
	for rows.Next() {
		err = rows.Scan(
			&dbo.ID,
			&dbo.Name,
		)
		if err != nil {
			return nil, err
		}
		dbo.Options = options[dbo.ID]
		result = append(result, dbo)
	}

	return result, nil
}

func (s *storage) getAllOptions(ctx context.Context) (map[int64][]repo.FullAnalyticsTypeOptions, error) {
	qb := squirrel.Select(
		"id",
		"type_id",
		"name",
	).From("analytics").PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	result := map[int64][]repo.FullAnalyticsTypeOptions{}
	var (
		option repo.FullAnalyticsTypeOptions
		id     int64
	)
	for rows.Next() {
		err = rows.Scan(
			&option.ID,
			&id,
			&option.Name,
		)
		if err != nil {
			return nil, err
		}

		result[id] = append(result[id], option)
	}

	return result, nil
}

func (s *storage) GetAnalyticsType(ctx context.Context, id int64) (repo.FullAnalyticsType, error) {
	qb := squirrel.Select(
		"name",
	).From("analytic_types").
		Where(squirrel.Eq{"id": id}).
		Limit(1).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return repo.FullAnalyticsType{}, err
	}

	row := s.database.QueryRowContext(ctx, query, args...)
	result := repo.FullAnalyticsType{ID: id}
	err = row.Scan(&result.Name)
	switch {
	case errors.Is(err, sql.ErrNoRows):
		return repo.FullAnalyticsType{}, repo.ErrNotFound
	case err != nil:
		return repo.FullAnalyticsType{}, err
	}

	qb = squirrel.Select(
		"id",
		"name",
	).From("analytics").
		Where(squirrel.Eq{"type_id": id}).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err = qb.ToSql()
	if err != nil {
		return repo.FullAnalyticsType{}, err
	}

	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return repo.FullAnalyticsType{}, err
	}
	defer closeRows(rows)

	var option repo.FullAnalyticsTypeOptions
	for rows.Next() {
		err = rows.Scan(
			&option.ID,
			&option.Name,
		)
		if err != nil {
			return repo.FullAnalyticsType{}, err
		}
		result.Options = append(result.Options, option)
	}

	return result, nil
}

func (s *storage) UpdateAnalyticsType(ctx context.Context, analyticsType repo.FullAnalyticsType) error {
	err := analyticsType.Validate()
	if err != nil {
		return err
	}

	tx, err := s.database.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer rollbackTX(tx)

	qb := squirrel.Update("analytic_types").SetMap(
		map[string]interface{}{
			"name": analyticsType.Name,
		},
	).Where(squirrel.Eq{"id": analyticsType.ID}).PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	err = updateAnalyticsTypeOptions(ctx, tx, analyticsType)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func updateAnalyticsTypeOptions(ctx context.Context, tx *sql.Tx, analyticsType repo.FullAnalyticsType) error {
	qbDeleteOptions := squirrel.Delete("analytics").
		Where(squirrel.Eq{"type_id": analyticsType.ID}).
		PlaceholderFormat(squirrel.Dollar)

	query, args, err := qbDeleteOptions.ToSql()
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	var insertWithID, insertWithoutID []repo.FullAnalyticsTypeOptions

	for _, option := range analyticsType.Options {
		if option.ID == 0 {
			insertWithoutID = append(insertWithoutID, option)
		} else {
			insertWithID = append(insertWithID, option)
		}
	}

	if len(insertWithID) > 0 {
		qbInsertOptions := squirrel.Insert("analytics").Columns(
			"id",
			"name",
			"type_id",
		).PlaceholderFormat(squirrel.Dollar)
		for _, option := range insertWithID {
			qbInsertOptions = qbInsertOptions.Values(
				option.ID,
				option.Name,
				analyticsType.ID,
			)
		}
		query, args, err = qbInsertOptions.ToSql()
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return err
		}
	}

	if len(insertWithoutID) > 0 {
		qbInsertOptions := squirrel.Insert("analytics").Columns(
			"name",
			"type_id",
		).PlaceholderFormat(squirrel.Dollar)
		for _, option := range insertWithoutID {
			qbInsertOptions = qbInsertOptions.Values(
				option.Name,
				analyticsType.ID,
			)
		}
		query, args, err = qbInsertOptions.ToSql()
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx, query, args...)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *storage) CreateAnalyticsType(ctx context.Context, newType repo.AnalyticsType) error {
	qb := squirrel.Insert("analytic_types").Columns("name").Values(newType.Name).PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = s.database.DB.ExecContext(ctx, query, args...)

	return err
}

func (s *storage) UpsertCurrencies(ctx context.Context, currencies []repo.Currency) error {
	qb := squirrel.Insert("currency").Columns(
		"id",
		"instrument_id",
		"price",
	)

	for _, row := range currencies {
		qb = qb.Values(
			row.ID,
			row.InstrumentID,
			row.Price,
		)
	}
	qb = qb.Suffix(
		`ON CONFLICT (id) DO UPDATE 
		SET instrument_id = excluded.instrument_id,
		price = excluded.price`,
	).PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = s.database.ExecContext(ctx, query, args...)

	return err
}

func (s *storage) ListCurrencies(ctx context.Context) ([]repo.Currency, error) {
	qb := squirrel.Select(
		"id",
		"instrument_id",
		"price",
	).From("currency").PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	var (
		result []repo.Currency
		dbo    repo.Currency
	)
	for rows.Next() {
		err := rows.Scan(
			&dbo.ID,
			&dbo.InstrumentID,
			&dbo.Price,
		)
		if err != nil {
			return nil, err
		}
		result = append(result, dbo)
	}

	return result, nil
}

func (s *storage) ListAnalyticsTypes(ctx context.Context) ([]repo.AnalyticsType, error) {
	qb := squirrel.Select(
		"id",
		"name",
	).From("analytic_types").PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}
	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	var (
		result []repo.AnalyticsType
		dbo    repo.AnalyticsType
	)
	for rows.Next() {
		err = rows.Scan(
			&dbo.ID,
			&dbo.Name,
		)
		if err != nil {
			return nil, err
		}

		result = append(result, dbo)
	}

	return result, nil
}

func (s *storage) ListInstruments(ctx context.Context, filter repo.InstrumentFilter) ([]repo.Instrument, error) {
	qb := squirrel.Select(
		"id",
		"tiker",
		"name",
		"currency",
		"type",
		"lot",
	).From("instrument").PlaceholderFormat(squirrel.Dollar)

	if len(filter.IDs) != 0 {
		qb = qb.Where(squirrel.Eq{"id": filter.IDs})
	}

	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	var (
		instriment instrumentDBO
		result     []repo.Instrument
	)
	for rows.Next() {
		err = rows.Scan(
			&instriment.ID,
			&instriment.Tiker,
			&instriment.Name,
			&instriment.Currency,
			&instriment.InstrumentType,
			&instriment.Lot,
		)
		if err != nil {
			return nil, err
		}

		currentInstrument := repo.Instrument{
			ID:             instriment.ID,
			Tiker:          optional.From(instriment.Tiker),
			Name:           optional.From(instriment.Name),
			Currency:       optional.From(instriment.Currency),
			Lot:            optional.From(instriment.Lot),
			InstrumentType: optional.From(repo.InstrumentTypeFromDB(instriment.InstrumentType)),
		}

		result = append(result, currentInstrument)
	}

	return result, nil
}

func (s *storage) GetPortfolio(ctx context.Context) ([]repo.Position, error) {
	analytics, err := s.getPortfolioAnalytics(ctx)
	if err != nil {
		return nil, err
	}

	qb := squirrel.Select(
		"p.instrument_id",
		"p.current_price * p.quantity * c.price",
		"p.average_price * p.quantity * c.price",
		"i.type",
	).From("portfolio as p").
		InnerJoin("instrument as i on p.instrument_id = i.id").
		InnerJoin("currency as c on i.currency = c.id").
		PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.database.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	var (
		position             repo.Position
		instrumentTypeString string
	)
	positionsMap := map[string]repo.Position{}
	for rows.Next() {
		err = rows.Scan(
			&position.InstrumentID,
			&position.CurrentSum,
			&position.BuySum,
			&instrumentTypeString,
		)
		if err != nil {
			return nil, err
		}
		position.InstrumentType = repo.InstrumentTypeFromDB(instrumentTypeString)

		existedPosition, ok := positionsMap[position.InstrumentID]
		if ok {
			existedPosition.BuySum += position.BuySum
			existedPosition.CurrentSum += position.CurrentSum
		} else {
			existedPosition = position
		}
		positionsMap[position.InstrumentID] = existedPosition
	}

	positions := make([]repo.Position, 0, len(positionsMap))
	for _, position := range positionsMap {
		position.Analytics = analytics[position.InstrumentID]
		positions = append(positions, position)
	}

	return positions, nil
}

func (s *storage) getPortfolioAnalytics(ctx context.Context) (map[string][]repo.Analytic, error) {
	qb := squirrel.Select(
		"av.analytic_types",
		"av.instrument_id",
		"av.analytic_id",
		"a.name",
	).From("analytic_value as av").
		InnerJoin("analytics as a ON (av.analytic_id = a.id)").
		PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.database.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer closeRows(rows)

	type dboAnalytic struct {
		AnalyticsType int64
		InstrumentID  string
		AnalyticID    int64
		Name          string
	}
	var dbo dboAnalytic
	result := make(map[string][]repo.Analytic)
	for rows.Next() {
		err = rows.Scan(
			&dbo.AnalyticsType,
			&dbo.InstrumentID,
			&dbo.AnalyticID,
			&dbo.Name,
		)
		if err != nil {
			return nil, err
		}

		analytics := result[dbo.InstrumentID]
		analytics = append(analytics, repo.Analytic{
			ID:    dbo.AnalyticID,
			Owner: dbo.AnalyticsType,
			Name:  dbo.Name,
		})
		result[dbo.InstrumentID] = analytics
	}

	return result, nil
}

func (s *storage) UpsertAccount(ctx context.Context, account repo.Account) error {
	qb := squirrel.Insert("account").SetMap(
		map[string]interface{}{
			"id":   account.ID,
			"name": account.Name,
		},
	).PlaceholderFormat(squirrel.Dollar).Suffix(
		"ON CONFLICT (id) DO UPDATE SET name = excluded.name",
	)

	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = s.database.ExecContext(ctx, query, args...)

	return err
}

func (s *storage) UpdatePortfolio(ctx context.Context, portfolio []repo.Portfolio) error {
	tx, err := s.database.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer rollbackTX(tx)

	_, err = tx.ExecContext(ctx, "DELETE FROM portfolio")
	if err != nil {
		return err
	}

	qb := squirrel.Insert("portfolio").Columns(
		"instrument_id",
		"account_id",
		"current_price",
		"average_price",
		"quantity",
	).PlaceholderFormat(squirrel.Dollar)

	for _, position := range portfolio {
		qb = qb.Values(
			position.InstrumentID,
			position.AccountID,
			position.CurrentPrice,
			position.AveragePrice,
			position.Quantity,
		)
	}

	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (s *storage) UpsertInstrument(ctx context.Context, instrument repo.Instrument) error {
	if instrument.ID == "" {
		return fmt.Errorf("%w: %s", repo.ErrInvalidData, "instrument must have ID")
	}

	exist, err := s.instrumentExist(ctx, instrument.ID)
	if err != nil {
		return err
	}

	if exist {
		return s.updateInstrument(ctx, instrument)
	}

	return s.insertInstrument(ctx, instrument)
}

func (s *storage) insertInstrument(ctx context.Context, instrument repo.Instrument) error {
	qb := squirrel.Insert("instrument")
	valsToSet := map[string]interface{}{
		"id": instrument.ID,
	}

	if instrument.Tiker.Valid {
		valsToSet["tiker"] = instrument.Tiker.Value
	}

	if instrument.Name.Valid {
		valsToSet["name"] = instrument.Name.Value
	}

	if instrument.InstrumentType.Valid {
		valsToSet["type"] = instrument.InstrumentType.Value.ToDB()
	}

	if instrument.Currency.Valid {
		valsToSet["currency"] = instrument.Currency.Value
	}

	if instrument.Lot.Valid {
		valsToSet["lot"] = instrument.Lot.Value
	}

	qb = qb.SetMap(valsToSet).PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = s.database.ExecContext(ctx, query, args...)

	return err
}

func (s *storage) updateInstrument(ctx context.Context, instrument repo.Instrument) error {
	qb := squirrel.Update("instrument")

	valsToSet := map[string]interface{}{}

	if instrument.Tiker.Valid {
		valsToSet["tiker"] = instrument.Tiker.Value
	}

	if instrument.Name.Valid {
		valsToSet["name"] = instrument.Name.Value
	}

	if instrument.InstrumentType.Valid {
		valsToSet["type"] = instrument.InstrumentType.Value.ToDB()
	}

	if instrument.Currency.Valid {
		valsToSet["currency"] = instrument.Currency.Value
	}

	if instrument.Lot.Valid {
		valsToSet["lot"] = instrument.Lot.Value
	}

	if len(valsToSet) == 0 {
		return nil
	}

	qb = qb.SetMap(valsToSet).Where(squirrel.Eq{"id": instrument.ID}).PlaceholderFormat(squirrel.Dollar)

	query, args, err := qb.ToSql()
	if err != nil {
		return err
	}

	_, err = s.database.ExecContext(ctx, query, args...)

	return err
}

func (s *storage) instrumentExist(ctx context.Context, id string) (bool, error) {
	qb := squirrel.Select("id").From("instrument").Where(squirrel.Eq{"id": id}).PlaceholderFormat(squirrel.Dollar)
	query, args, err := qb.ToSql()
	if err != nil {
		return false, err
	}

	row := s.database.QueryRowContext(ctx, query, args...)
	var scanned string
	err = row.Scan(&scanned)
	switch {
	case errors.Is(err, sql.ErrNoRows):
		return false, nil
	case err != nil:
		return false, err
	}

	return true, nil
}

func closeRows(rows *sql.Rows) {
	err := rows.Close()
	if err != nil {
		log.Println(err)
	}
}

func rollbackTX(tx *sql.Tx) {
	err := tx.Rollback()
	if err != nil && !errors.Is(err, sql.ErrTxDone) {
		log.Println(err)
	}
}

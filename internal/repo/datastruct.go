package repo

import (
	"context"
	"database/sql"
	"errors"

	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/common/optional"
)

var (
	ErrInvalidData = errors.New("invalid data")
	ErrNotFound    = errors.New("not found")
)

type Storage interface {
	// GetPortfolio возвращает текущее состояние счета сгруппированное по бумагам
	GetPortfolio(ctx context.Context) ([]Position, error)
	// UpdatePortfolio обновляет список всех позиций, затирая старые
	UpdatePortfolio(ctx context.Context, portfolio []Portfolio) error
	// UpsertAccount создает/обновляет счет
	UpsertAccount(ctx context.Context, account Account) error
	// ListInstruments получает список всех инструментов
	ListInstruments(ctx context.Context, filter InstrumentFilter) ([]Instrument, error)
	// UpsertInstrument создает инструмент или обновляет существующий
	UpsertInstrument(ctx context.Context, instrument Instrument) error
	// ListAnalyticTypes возвращает типы аналитик
	ListAnalyticsTypes(ctx context.Context) ([]AnalyticsType, error)
	// ListCurrencies получает валюты
	ListCurrencies(ctx context.Context) ([]Currency, error)
	// UpsertCurrencies записывает валюты
	UpsertCurrencies(ctx context.Context, currencies []Currency) error
	// CreateAnalyticsType создает новый тип аналитики
	CreateAnalyticsType(ctx context.Context, newType AnalyticsType) error
	// GetAnalyticsType получает тип аналитики по ID
	GetAnalyticsType(ctx context.Context, id int64) (FullAnalyticsType, error)
	// ListAnalyticsTypes получает тип аналитики по ID
	ListFullAnalyticsTypes(ctx context.Context) ([]FullAnalyticsType, error)
	// UpdateAnalyticsType сохраняетсуществующую аналитику
	UpdateAnalyticsType(ctx context.Context, analyticsType FullAnalyticsType) error
	// GetFullInstrument получает инструмент со всеми его данными
	GetFullInstrument(ctx context.Context, id string) (FullInstrument, error)
	// UpdateFullInstrument обновляет существующий инструмент
	UpdateFullInstrument(ctx context.Context, instrument FullInstrument) error
}

const (
	DBTypePostgres = "postgres"
	DBTypeSQLite   = "sqlite"
)

type Instrument struct {
	ID             string
	Tiker          optional.Optional[string]
	Name           optional.Optional[string]
	Currency       optional.Optional[string]
	Lot            optional.Optional[int32]
	InstrumentType optional.Optional[InstrumentType]
}

type Portfolio struct {
	InstrumentID string
	AccountID    string
	CurrentPrice float64
	AveragePrice float64
	Quantity     float64
}

type Position struct {
	InstrumentID   string
	CurrentSum     float64
	BuySum         float64
	Analytics      []Analytic
	InstrumentType InstrumentType
}

type AnalyticsType struct {
	ID   int64
	Name string
}

type FullInstrument struct {
	Instrument Instrument
	Analytics  map[int64]int64
}

type FullAnalyticsType struct {
	ID      int64
	Name    string
	Options []FullAnalyticsTypeOptions
}

type FullAnalyticsTypeOptions struct {
	ID   int64
	Name string
}

type Currency struct {
	ID           string
	InstrumentID string
	Price        sql.NullFloat64
}

type Analytic struct {
	ID    int64
	Owner int64
	Name  string
}

type Account struct {
	ID   string
	Name string
}

type InstrumentFilter struct {
	IDs []string
}

type InstrumentType int8

const (
	InstrumentTypeUndefined InstrumentType = iota
	InstrumentTypeShare
	InstrumentTypeBond
	InstrumentTypeEtf
	InstrumentTypeCurrency
)

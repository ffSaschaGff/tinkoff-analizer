package tinkoffapi

import investapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/pkg/tinkoff_api"

func accountFromProto(in *investapi.Account) Account {
	return Account{
		ID:          in.GetId(),
		Description: in.GetName(),
	}
}

func portfolioPositionFromProto(in *investapi.PortfolioPosition, account string) PortfolioPosition {
	return PortfolioPosition{
		InstrumentID:   in.GetInstrumentUid(),
		AccountID:      account,
		InstrumentType: instrumentTypeFromProto(in.GetInstrumentType()),
		Quantity:       quotationFromProto(in.GetQuantity()),
		Blocked:        quotationFromProto(in.GetBlockedLots()),
		PositionPrice:  moneyValueFromProto(in.GetAveragePositionPrice()),
		CurrentPrice:   moneyValueFromProto(in.GetCurrentPrice()),
	}
}

func instrumentTypeFromProto(in string) InstrumentType {
	switch in {
	case "bond":
		return InstrumentTypeBond
	case "currency":
		return InstrumentTypeCurrency
	case "etf":
		return InstrumentTypeEtf
	case "share":
		return InstrumentTypeShare
	}

	return InstrumentTypeUndefined
}

func quotationFromProto(in *investapi.Quotation) Quotation {
	return Quotation{
		Units: in.GetUnits(),
		Nano:  in.GetNano(),
	}
}

func moneyValueFromProto(in *investapi.MoneyValue) Quotation {
	return Quotation{
		Units: in.GetUnits(),
		Nano:  in.GetNano(),
	}
}

package tinkoffapi

type Account struct {
	ID          string
	Description string
}

type Instrument struct {
	ID       string
	Tiker    string
	Name     string
	Currency string
	Lot      int32
	Type     InstrumentType
}

type PortfolioPosition struct {
	InstrumentID   string
	AccountID      string
	InstrumentType InstrumentType
	Quantity       Quotation
	Blocked        Quotation
	PositionPrice  Quotation
	CurrentPrice   Quotation
}

type Quotation struct {
	Units int64
	Nano  int32
}

type InstrumentType int8

const (
	InstrumentTypeUndefined InstrumentType = iota
	InstrumentTypeShare
	InstrumentTypeBond
	InstrumentTypeEtf
	InstrumentTypeCurrency
)

func (quotation Quotation) Float64() float64 {
	nano := 1000000000.0

	return float64(quotation.Units) + (float64(quotation.Nano) / nano)
}

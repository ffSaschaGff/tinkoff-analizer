package tinkoffapi

import (
	"context"
	"crypto/tls"
	"fmt"
	"math"
	"time"

	"github.com/google/uuid"
	investapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/pkg/tinkoff_api"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	grpcMetadata "google.golang.org/grpc/metadata"
)

const (
	appName                      = "ffsaschagff/tinkoff-analizer"
	defaultRequestTimeoutSeconds = 60
	maxCallRecvMsgSize           = math.MaxInt
)

type Client interface {
	// GetAccounts получает счета пользователя
	GetAccounts(ctx context.Context) ([]Account, error)
	// GetInstruments получает все инструменты (акции, облигации, валюты, ETF)
	GetInstruments(ctx context.Context) ([]Instrument, error)
	// GetPortfolio получает портфель по счету
	GetPortfolio(ctx context.Context, accountID string) ([]PortfolioPosition, error)
	// GetCurrentPrices получает прайсы по id инструментов
	GetCurrentPrices(ctx context.Context, ids []string) (map[string]Quotation, error)
}

type client struct {
	operationService   investapi.OperationsServiceClient
	usersService       investapi.UsersServiceClient
	instrumentsService investapi.InstrumentsServiceClient
	marketdataService  investapi.MarketDataServiceClient
	token              string
}

const host = "invest-public-api.tinkoff.ru:443"

func NewClient(token string) (Client, error) {
	tlsConfig := tls.Config{MinVersion: tls.VersionTLS12}
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(credentials.NewTLS(&tlsConfig)),
	}
	conn, err := grpc.Dial(host, opts...)
	if err != nil {
		return nil, err
	}

	return &client{
		operationService:   investapi.NewOperationsServiceClient(conn),
		usersService:       investapi.NewUsersServiceClient(conn),
		instrumentsService: investapi.NewInstrumentsServiceClient(conn),
		marketdataService:  investapi.NewMarketDataServiceClient(conn),
		token:              token,
	}, nil
}

func (c *client) GetCurrentPrices(ctx context.Context, ids []string) (map[string]Quotation, error) {
	ctx, cancel := c.createRequestContext(ctx)
	defer cancel()

	res, err := c.marketdataService.GetLastPrices(
		ctx,
		&investapi.GetLastPricesRequest{
			InstrumentId: ids,
		},
	)
	if err != nil {
		return nil, err
	}

	result := map[string]Quotation{}
	for _, price := range res.GetLastPrices() {
		result[price.GetInstrumentUid()] = Quotation{
			Units: price.Price.GetUnits(),
			Nano:  price.Price.GetNano(),
		}
	}

	return result, nil
}

func (c *client) GetAccounts(ctx context.Context) ([]Account, error) {
	ctx, cancel := c.createRequestContext(ctx)
	defer cancel()

	res, err := c.usersService.GetAccounts(
		ctx,
		&investapi.GetAccountsRequest{},
	)

	if err != nil {
		return nil, err
	}

	result := make([]Account, len(res.GetAccounts()))
	for i, account := range res.GetAccounts() {
		result[i] = accountFromProto(account)
	}

	return result, nil
}

func (c *client) GetPortfolio(ctx context.Context, accountID string) ([]PortfolioPosition, error) {
	ctx, cancel := c.createRequestContext(ctx)
	defer cancel()

	res, err := c.operationService.GetPortfolio(
		ctx,
		&investapi.PortfolioRequest{
			AccountId: accountID,
			Currency:  investapi.PortfolioRequest_RUB,
		},
	)

	if err != nil {
		return nil, err
	}

	result := make([]PortfolioPosition, len(res.GetPositions()))
	for i, position := range res.GetPositions() {
		result[i] = portfolioPositionFromProto(position, accountID)
	}

	return result, nil
}

func (c *client) GetInstruments(ctx context.Context) ([]Instrument, error) {
	ctx, cancel := c.createRequestContext(ctx)
	defer cancel()

	instrumentsCh := make(chan []Instrument, 4)

	errGroup, ctxGroup := errgroup.WithContext(ctx)
	instrumentAPIs := []func(ctx context.Context) ([]Instrument, error){
		c.getBonds,
		c.getCurrencies,
		c.getETFs,
		c.getShares,
	}

	for _, oneHandler := range instrumentAPIs {
		errGroup.Go(func(handler func(ctx context.Context) ([]Instrument, error)) func() error {
			return func() error {
				instruments, err := handler(ctxGroup)
				if err != nil {
					return err
				}

				instrumentsCh <- instruments

				return nil
			}
		}(oneHandler))
	}

	err := errGroup.Wait()
	close(instrumentsCh)
	if err != nil {
		return nil, err
	}

	var result []Instrument
	for instruments := range instrumentsCh {
		result = append(result, instruments...)
	}

	return result, nil
}

func (c *client) getBonds(ctx context.Context) ([]Instrument, error) {
	res, err := c.instrumentsService.Bonds(
		ctx,
		&investapi.InstrumentsRequest{
			InstrumentStatus: investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL,
		},
		grpc.MaxCallRecvMsgSize(maxCallRecvMsgSize),
	)

	if err != nil {
		return nil, err
	}

	result := make([]Instrument, len(res.GetInstruments()))
	for i, instrument := range res.GetInstruments() {
		result[i] = Instrument{
			ID:       instrument.GetUid(),
			Tiker:    instrument.GetTicker(),
			Name:     instrument.GetName(),
			Type:     InstrumentTypeBond,
			Currency: instrument.GetCurrency(),
			Lot:      instrument.GetLot(),
		}
	}

	return result, nil
}

func (c *client) getETFs(ctx context.Context) ([]Instrument, error) {
	res, err := c.instrumentsService.Etfs(
		ctx,
		&investapi.InstrumentsRequest{
			InstrumentStatus: investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL,
		},
		grpc.MaxCallRecvMsgSize(maxCallRecvMsgSize),
	)

	if err != nil {
		return nil, err
	}

	result := make([]Instrument, len(res.GetInstruments()))
	for i, instrument := range res.GetInstruments() {
		result[i] = Instrument{
			ID:       instrument.GetUid(),
			Tiker:    instrument.GetTicker(),
			Name:     instrument.GetName(),
			Type:     InstrumentTypeEtf,
			Currency: instrument.GetCurrency(),
			Lot:      instrument.GetLot(),
		}
	}

	return result, nil
}

func (c *client) getCurrencies(ctx context.Context) ([]Instrument, error) {
	res, err := c.instrumentsService.Currencies(
		ctx,
		&investapi.InstrumentsRequest{
			InstrumentStatus: investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL,
		},
		grpc.MaxCallRecvMsgSize(maxCallRecvMsgSize),
	)

	if err != nil {
		return nil, err
	}

	result := make([]Instrument, len(res.GetInstruments()))
	for i, instrument := range res.GetInstruments() {
		result[i] = Instrument{
			ID:       instrument.GetUid(),
			Tiker:    instrument.GetTicker(),
			Name:     instrument.GetName(),
			Type:     InstrumentTypeCurrency,
			Currency: instrument.GetCurrency(),
			Lot:      instrument.GetLot(),
		}
	}

	return result, nil
}

func (c *client) getShares(ctx context.Context) ([]Instrument, error) {
	res, err := c.instrumentsService.Shares(
		ctx,
		&investapi.InstrumentsRequest{
			InstrumentStatus: investapi.InstrumentStatus_INSTRUMENT_STATUS_ALL,
		},
		grpc.MaxCallRecvMsgSize(maxCallRecvMsgSize),
	)

	if err != nil {
		return nil, err
	}

	result := make([]Instrument, len(res.GetInstruments()))
	for i, instrument := range res.GetInstruments() {
		result[i] = Instrument{
			ID:       instrument.GetUid(),
			Tiker:    instrument.GetTicker(),
			Name:     instrument.GetName(),
			Type:     InstrumentTypeShare,
			Currency: instrument.GetCurrency(),
			Lot:      instrument.GetLot(),
		}
	}

	return result, nil
}

func (c *client) createRequestContext(ctx context.Context) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, time.Second*defaultRequestTimeoutSeconds)

	authHeader := fmt.Sprintf("Bearer %s", c.token)
	ctx = grpcMetadata.AppendToOutgoingContext(ctx, "authorization", authHeader)
	ctx = grpcMetadata.AppendToOutgoingContext(ctx, "x-tracking-id", uuid.New().String())
	ctx = grpcMetadata.AppendToOutgoingContext(ctx, "x-app-name", appName)

	return ctx, cancel
}

package selfapi

import (
	"database/sql"
	"errors"
	"net/http"

	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type ErrMessage struct {
	Error string `json:"error"`
	Msg   string `json:"msg"`
}

func errorToFrontErr(err error) (int, ErrMessage) {
	result := ErrMessage{
		Error: err.Error(),
	}
	switch {
	case errors.Is(err, ErrValidation):
		result.Msg = "Невалиндый запрос"
		return http.StatusBadRequest, result
	case errors.Is(err, repo.ErrNotFound):
		result.Msg = "Объект не найден"
		return http.StatusNotFound, result
	case errors.Is(err, repo.ErrInvalidData):
		result.Msg = "Невалиндый запрос"
		return http.StatusBadRequest, result
	case errors.Is(err, sql.ErrNoRows):
		result.Msg = "Объект не найден"
		return http.StatusNotFound, result
	}

	result.Msg = err.Error()
	return http.StatusInternalServerError, result
}

package selfapi

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/config"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/service"
)

type SelfAPI interface {
	Run()
}

const apiPrefix = "/api_v1/"

type selfAPI struct {
	portfolioGetter   service.PortfolioGetter
	instrumentLoader  service.InstrumentsLoader
	instrumentService service.InstrumentService
	analyticsService  service.AnalyticsService
}

func NewSelfAPI(
	portfolioGetter service.PortfolioGetter,
	instrumentLoader service.InstrumentsLoader,
	instrumentService service.InstrumentService,
	analyticsService service.AnalyticsService,
) SelfAPI {
	return &selfAPI{
		portfolioGetter:   portfolioGetter,
		instrumentLoader:  instrumentLoader,
		instrumentService: instrumentService,
		analyticsService:  analyticsService,
	}
}

var ErrValidation = errors.New("invalid request")

func (s *selfAPI) Run() {
	router := gin.Default()
	router.Static("/static", "./static")
	router.GET(apiPrefix+"portfolio", s.getPortfolio)
	router.GET(apiPrefix+"instruments", s.listInstruments)
	router.GET(apiPrefix+"analytics_type", s.listAnalyticTypes)
	router.POST(apiPrefix+"load_portfolio", s.loadPortfolio)
	router.POST(apiPrefix+"analytics_type", s.createAnalyticsType)
	router.GET(apiPrefix+"analytics_type/:id", s.getAnalyticsType)
	router.PUT(apiPrefix+"analytics_type", s.updateAnalyticsType)
	router.GET(apiPrefix+"instruments/:id", s.getInstrument)
	router.PUT(apiPrefix+"instruments/:id/options", s.updateInstrumentOptions)
	err := router.Run(":" + config.GetValue(config.HTTPPort).String())
	if err != nil {
		log.Fatalln(err)
	}
}

func (s *selfAPI) updateInstrumentOptions(ctx *gin.Context) {
	type idGetReq struct {
		ID string `uri:"id" binding:"required"`
	}
	var id idGetReq

	if err := ctx.ShouldBindUri(&id); err != nil {
		ctx.JSON(400, err)
		return
	}

	bodyReader := ctx.Request.Body
	if bodyReader == nil {
		ctx.JSON(400, ErrValidation)
		return
	}
	body, err := io.ReadAll(bodyReader)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	var options []service.FullInstrumentOptions
	err = json.Unmarshal(body, &options)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	err = s.instrumentService.UpdateInstrumentOptions(ctx, id.ID, options)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, nil)
}

func (s *selfAPI) getInstrument(ctx *gin.Context) {
	type idGetReq struct {
		ID string `uri:"id" binding:"required"`
	}
	var req idGetReq

	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(errorToFrontErr(ErrValidation))
		return
	}

	analyticsType, err := s.instrumentService.GetInstrument(ctx, req.ID)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, analyticsType)
}

func (s *selfAPI) getAnalyticsType(ctx *gin.Context) {
	type idGetReq struct {
		ID int64 `uri:"id" binding:"required"`
	}
	var req idGetReq

	if err := ctx.ShouldBindUri(&req); err != nil {
		ctx.JSON(errorToFrontErr(ErrValidation))
		return
	}

	analyticsType, err := s.analyticsService.GetAnalyticsType(ctx, req.ID)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, analyticsType)
}

func (s *selfAPI) updateAnalyticsType(ctx *gin.Context) {
	bodyReader := ctx.Request.Body
	if bodyReader == nil {
		ctx.JSON(errorToFrontErr(ErrValidation))
		return
	}
	body, err := io.ReadAll(bodyReader)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}
	analyticsType := service.FullAnalyticsType{}
	err = json.Unmarshal(body, &analyticsType)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	err = s.analyticsService.UpdateAnalyticsType(ctx, analyticsType)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, nil)
}

func (s *selfAPI) createAnalyticsType(ctx *gin.Context) {
	bodyReader := ctx.Request.Body
	if bodyReader == nil {
		ctx.JSON(errorToFrontErr(ErrValidation))
		return
	}
	body, err := io.ReadAll(bodyReader)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}
	analyticsType := service.AnalyticsType{}
	err = json.Unmarshal(body, &analyticsType)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	err = s.analyticsService.CreateAnalyticsType(ctx, analyticsType)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, nil)
}

func (s *selfAPI) loadPortfolio(ctx *gin.Context) {
	err := s.instrumentLoader.LoadInstruments(ctx)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, nil)
}

func (s *selfAPI) listAnalyticTypes(ctx *gin.Context) {
	types, err := s.analyticsService.ListAnalyticTypes(ctx)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, types)
}

func (s *selfAPI) listInstruments(ctx *gin.Context) {
	body, err := url.QueryUnescape(ctx.Request.URL.RawQuery)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}
	filter, err := url.ParseQuery(body)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ids := strings.Split(filter.Get("IDs"), ",")

	instruments, err := s.instrumentService.ListInstruments(ctx, ids)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, instruments)
}

func (s *selfAPI) getPortfolio(ctx *gin.Context) {
	portfolio, err := s.portfolioGetter.GetPortfolio(ctx)
	if err != nil {
		ctx.JSON(errorToFrontErr(err))
		return
	}

	ctx.JSON(200, portfolio)
}

package config

import (
	"flag"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/joho/godotenv"
)

type Value string

const (
	DatabaseUser            = "DB_LOGIN"
	DatabasePassword        = "DB_PASSWORD"
	DatabaseName            = "DB_NAME"
	DatabaseHost            = "DB_HOST"
	DatabaseMaxConns        = "DB_MAX_CONNS"
	DatabaseConnLifeTimeMin = "DB_CONN_LIFETIME_MIN"
	TinkoffToken            = "TINKOFF_TOKEN" //nolint:all
	HTTPPort                = "HTTP_PORT"
	DatabaseType            = "DB_TYPE"
	SQLiteConn              = "SQLITE_CONN"
	OpenOnStart             = "OPEN_ON_START"
)

var (
	defValues = map[string]string{
		DatabaseConnLifeTimeMin: "5",
		DatabaseMaxConns:        "5",
		DatabaseHost:            "localhost",
		HTTPPort:                "9001",
		DatabaseType:            "sqlite",
		SQLiteConn:              "storage.db",
		OpenOnStart:             "true",
	}

	cmdValues = map[string]string{}

	onceCmdInit = &sync.Once{}
)

func GetValue(key string) Value {
	onceCmdInit.Do(initValues)

	value := cmdValues[key]
	if value != "" {
		return Value(value)
	}

	value, ok := os.LookupEnv(key)
	if ok {
		return Value(value)
	}

	return Value(defValues[key])
}

func initValues() {
	source := []string{
		DatabaseUser,
		DatabasePassword,
		DatabaseName,
		DatabaseHost,
		DatabaseMaxConns,
		DatabaseConnLifeTimeMin,
		TinkoffToken,
		HTTPPort,
		DatabaseType,
		SQLiteConn,
	}
	values := make([]string, len(source))

	for i, configKey := range source {
		flag.StringVar(&values[i], strings.ToLower(configKey), "", "")
	}
	flag.Parse()

	for i := range source {
		cmdValues[source[i]] = values[i]
	}

	if err := godotenv.Load(); err != nil {
		log.Println("has no .env")
	}
}

func (value Value) String() string {
	return string(value)
}

func (value Value) Int64() int64 {
	result, _ := strconv.ParseInt(value.String(), 10, 64)

	return result
}

func (value Value) Int() int {
	result, _ := strconv.Atoi(value.String())

	return result
}

func (value Value) Bool() bool {
	return value.String() == "true"
}

package service

import (
	"context"
	"database/sql"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	tinkoffapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/client/tinkoff_api"
	mock_api "gitlab.com/ffsaschagff/tinkoff-analizer/internal/client/tinkoff_api/mock"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/common/optional"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
	mock_storage "gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo/mock"
)

var errSome = errors.New("some err")

func Test_instrumentsLoader_LoadInstruments(t *testing.T) {
	t.Parallel()

	type fields struct {
		storage repo.Storage
		tinkoff tinkoffapi.Client
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "valid case",
			fields: fields{
				storage: func() repo.Storage {
					m := mock_storage.NewMockStorage(gomock.NewController(t))
					m.EXPECT().UpsertInstrument(
						context.Background(),
						repo.Instrument{
							ID:             "123-456",
							Tiker:          optional.From("SBER"),
							Name:           optional.From("Сбербанк"),
							Currency:       optional.From("rub"),
							Lot:            optional.From(int32(1)),
							InstrumentType: optional.From(repo.InstrumentTypeShare),
						},
					).Return(nil)
					m.EXPECT().UpsertInstrument(
						context.Background(),
						repo.Instrument{
							ID:             "123-457",
							Tiker:          optional.From("MAGN-01"),
							Name:           optional.From("Магнит облигации"),
							Currency:       optional.From("rub"),
							Lot:            optional.From(int32(1)),
							InstrumentType: optional.From(repo.InstrumentTypeBond),
						},
					).Return(nil)
					m.EXPECT().UpsertInstrument(
						context.Background(),
						repo.Instrument{
							ID:             "123-458",
							Tiker:          optional.From("MAGN"),
							Name:           optional.From("Магнит"),
							Currency:       optional.From("rub"),
							Lot:            optional.From(int32(1)),
							InstrumentType: optional.From(repo.InstrumentTypeShare),
						},
					).Return(nil)
					m.EXPECT().UpsertAccount(
						context.Background(),
						repo.Account{
							ID:   "100200",
							Name: "Инвесткопилка",
						},
					).Return(nil)
					m.EXPECT().UpsertAccount(
						context.Background(),
						repo.Account{
							ID:   "100201",
							Name: "Брокерский счет",
						},
					).Return(nil)
					m.EXPECT().UpdatePortfolio(
						context.Background(),
						[]repo.Portfolio{
							{
								InstrumentID: "123-456",
								AccountID:    "100201",
								Quantity:     1,
								AveragePrice: 100,
								CurrentPrice: 110,
							},
							{
								InstrumentID: "123-457",
								AccountID:    "100201",
								Quantity:     1,
								AveragePrice: 100,
								CurrentPrice: 90,
							},
						},
					).Return(nil)
					m.EXPECT().ListCurrencies(
						context.Background(),
					).Return(
						[]repo.Currency{
							{
								ID:           "rub",
								InstrumentID: "111-001",
								Price: sql.NullFloat64{
									Valid:   true,
									Float64: 1,
								},
							},
							{
								ID:           "usd",
								InstrumentID: "111-002",
								Price: sql.NullFloat64{
									Valid:   true,
									Float64: 30,
								},
							},
						},
						nil,
					)
					m.EXPECT().UpsertCurrencies(
						context.Background(),
						[]repo.Currency{
							{
								ID:           "rub",
								InstrumentID: "111-001",
								Price: sql.NullFloat64{
									Valid:   true,
									Float64: 1,
								},
							},
							{
								ID:           "usd",
								InstrumentID: "111-002",
								Price: sql.NullFloat64{
									Valid:   true,
									Float64: 90,
								},
							},
						},
					)
					return m
				}(),
				tinkoff: func() tinkoffapi.Client {
					m := mock_api.NewMockClient(gomock.NewController(t))
					m.EXPECT().GetInstruments(context.Background()).Return(
						[]tinkoffapi.Instrument{
							{
								ID:       "123-456",
								Tiker:    "SBER",
								Name:     "Сбербанк",
								Currency: "rub",
								Lot:      1,
								Type:     tinkoffapi.InstrumentTypeShare,
							},
							{
								ID:       "123-457",
								Tiker:    "MAGN-01",
								Name:     "Магнит облигации",
								Currency: "rub",
								Lot:      1,
								Type:     tinkoffapi.InstrumentTypeBond,
							},
							{
								ID:       "123-458",
								Tiker:    "MAGN",
								Name:     "Магнит",
								Currency: "rub",
								Lot:      1,
								Type:     tinkoffapi.InstrumentTypeShare,
							},
						},
						nil,
					)
					m.EXPECT().GetAccounts(context.Background()).Return(
						[]tinkoffapi.Account{
							{
								ID:          "100200",
								Description: "Инвесткопилка",
							},
							{
								ID:          "100201",
								Description: "Брокерский счет",
							},
						},
						nil,
					)
					m.EXPECT().GetPortfolio(
						context.Background(),
						"100200",
					).Return(
						nil,
						errSome,
					)
					m.EXPECT().GetPortfolio(
						context.Background(),
						"100201",
					).Return(
						[]tinkoffapi.PortfolioPosition{
							{
								InstrumentID:   "123-456",
								AccountID:      "100201",
								InstrumentType: tinkoffapi.InstrumentTypeShare,
								Quantity:       tinkoffapi.Quotation{Units: 1},
								PositionPrice:  tinkoffapi.Quotation{Units: 100},
								CurrentPrice:   tinkoffapi.Quotation{Units: 110},
							},
							{
								InstrumentID:   "123-457",
								AccountID:      "100201",
								InstrumentType: tinkoffapi.InstrumentTypeBond,
								Quantity:       tinkoffapi.Quotation{Units: 1},
								PositionPrice:  tinkoffapi.Quotation{Units: 100},
								CurrentPrice:   tinkoffapi.Quotation{Units: 90},
							},
						},
						nil,
					)
					m.EXPECT().GetCurrentPrices(
						context.Background(),
						[]string{"111-001", "111-002"},
					).Return(
						map[string]tinkoffapi.Quotation{
							"111-001": {},
							"111-002": {Units: 90},
						},
						nil,
					)
					return m
				}(),
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			err := NewInstrumentsLoader(
				tt.fields.storage,
				tt.fields.tinkoff,
			).LoadInstruments(context.Background())

			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}

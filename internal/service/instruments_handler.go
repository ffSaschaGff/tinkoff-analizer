package service

import (
	"context"

	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type InstrumentService interface {
	ListInstruments(ctx context.Context, ids []string) ([]Instrument, error)
	GetInstrument(ctx context.Context, id string) (FullInstrument, error)
	UpdateInstrumentOptions(ctx context.Context, id string, options []FullInstrumentOptions) error
}

type instrumentService struct {
	storage repo.Storage
}

func NewInstrumentService(storage repo.Storage) InstrumentService {
	return &instrumentService{
		storage: storage,
	}
}

func (s *instrumentService) UpdateInstrumentOptions(
	ctx context.Context,
	id string,
	options []FullInstrumentOptions,
) error {
	optionsToInsert := map[int64]int64{}
	for _, option := range options {
		optionsToInsert[option.Type] = option.Value
	}
	req := repo.FullInstrument{
		Instrument: repo.Instrument{
			ID: id,
		},
		Analytics: optionsToInsert,
	}
	return s.storage.UpdateFullInstrument(ctx, req)
}

func (s *instrumentService) GetInstrument(ctx context.Context, id string) (FullInstrument, error) {
	dbInstrument, err := s.storage.GetFullInstrument(ctx, id)
	if err != nil {
		return FullInstrument{}, err
	}

	result := FullInstrument{
		ID:             id,
		Tiker:          dbInstrument.Instrument.Tiker.Value,
		Name:           dbInstrument.Instrument.Name.Value,
		Currency:       dbInstrument.Instrument.Currency.Value,
		Lot:            dbInstrument.Instrument.Lot.Value,
		InstrumentType: TypeToBDO(dbInstrument.Instrument.InstrumentType.Value),
	}

	for analyticType, analyticValue := range dbInstrument.Analytics {
		result.Options = append(result.Options, FullInstrumentOptions{
			Type:  analyticType,
			Value: analyticValue,
		})
	}

	return result, nil
}

func (s *instrumentService) ListInstruments(ctx context.Context, ids []string) ([]Instrument, error) {
	instriments, err := s.storage.ListInstruments(ctx, repo.InstrumentFilter{IDs: ids})
	if err != nil {
		return nil, err
	}

	result := make([]Instrument, 0, len(instriments))
	for _, instrument := range instriments {
		result = append(result, Instrument{
			ID:             instrument.ID,
			Tiker:          instrument.Tiker.Value,
			Name:           instrument.Name.Value,
			Currency:       instrument.Currency.Value,
			Lot:            instrument.Lot.Value,
			InstrumentType: TypeToBDO(instrument.InstrumentType.Value),
		})
	}

	return result, nil
}

package service

import (
	"context"
	"database/sql"

	tinkoffapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/client/tinkoff_api"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type InstrumentsLoader interface {
	LoadInstruments(ctx context.Context) error
}

type instrumentsLoader struct {
	storage repo.Storage
	tinkoff tinkoffapi.Client
}

func NewInstrumentsLoader(
	storage repo.Storage,
	tinkoff tinkoffapi.Client,
) InstrumentsLoader {
	return &instrumentsLoader{
		storage: storage,
		tinkoff: tinkoff,
	}
}

func (h *instrumentsLoader) LoadInstruments(ctx context.Context) error {
	err := h.loadInstruments(ctx)
	if err != nil {
		return err
	}

	err = h.loadPortfolio(ctx)
	if err != nil {
		return err
	}

	err = h.loadCurrenciesExchange(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (h *instrumentsLoader) loadInstruments(ctx context.Context) error {
	instruments, err := h.tinkoff.GetInstruments(ctx)
	if err != nil {
		return err
	}

	for _, instrument := range instruments {
		err = h.storage.UpsertInstrument(
			ctx,
			apiInstrumentToDB(instrument),
		)

		if err != nil {
			return err
		}
	}

	return nil
}

func (h *instrumentsLoader) loadPortfolio(ctx context.Context) error {
	accounts, err := h.tinkoff.GetAccounts(ctx)
	if err != nil {
		return err
	}

	var portfolioToUpdate []repo.Portfolio
	for _, account := range accounts {
		err = h.storage.UpsertAccount(
			ctx,
			repo.Account{
				ID:   account.ID,
				Name: account.Description,
			},
		)

		if err != nil {
			return err
		}

		var portfolio []tinkoffapi.PortfolioPosition
		portfolio, err = h.tinkoff.GetPortfolio(ctx, account.ID)
		if err != nil {
			// TODO: проверять что за статус InvalidArguments можно пропускать
			continue
		}

		for _, position := range portfolio {
			portfolioToUpdate = append(portfolioToUpdate,
				repo.Portfolio{
					InstrumentID: position.InstrumentID,
					AccountID:    position.AccountID,
					CurrentPrice: position.CurrentPrice.Float64(),
					AveragePrice: position.PositionPrice.Float64(),
					Quantity:     position.Quantity.Float64(),
				},
			)
		}
	}

	return h.storage.UpdatePortfolio(
		ctx,
		portfolioToUpdate,
	)
}

func (h *instrumentsLoader) loadCurrenciesExchange(ctx context.Context) error {
	currencies, err := h.storage.ListCurrencies(ctx)
	if err != nil {
		return err
	}
	ids := make([]string, 0, len(currencies))
	for _, currency := range currencies {
		ids = append(ids, currency.InstrumentID)
	}
	prices, err := h.tinkoff.GetCurrentPrices(ctx, ids)
	if err != nil {
		return err
	}
	for i := range currencies {
		if currencies[i].ID == "rub" {
			currencies[i].Price = sql.NullFloat64{Valid: true, Float64: 1}
			continue
		}
		current, ok := prices[currencies[i].InstrumentID]
		if !ok {
			currencies[i].Price = sql.NullFloat64{Valid: true}
		}

		currencies[i].Price = sql.NullFloat64{Valid: true, Float64: current.Float64()}
	}

	return h.storage.UpsertCurrencies(ctx, currencies)
}

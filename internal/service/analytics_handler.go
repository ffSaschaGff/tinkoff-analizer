package service

import (
	"context"

	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type AnalyticsService interface {
	ListAnalyticTypes(ctx context.Context) ([]FullAnalyticsType, error)
	CreateAnalyticsType(ctx context.Context, newAnalytic AnalyticsType) error
	GetAnalyticsType(ctx context.Context, id int64) (FullAnalyticsType, error)
	UpdateAnalyticsType(ctx context.Context, analyticsType FullAnalyticsType) error
}
type analyticsService struct {
	storage repo.Storage
}

func NewAnalyticsService(storage repo.Storage) AnalyticsService {
	return &analyticsService{storage: storage}
}

func (h *analyticsService) ListAnalyticTypes(ctx context.Context) ([]FullAnalyticsType, error) {
	types, err := h.storage.ListFullAnalyticsTypes(ctx)
	if err != nil {
		return nil, err
	}

	result := make([]FullAnalyticsType, 0, len(types))
	for _, oneType := range types {
		var options []FullAnalyticsTypeOptions
		for _, oneOption := range oneType.Options {
			options = append(options, FullAnalyticsTypeOptions{
				ID:   oneOption.ID,
				Name: oneOption.Name,
			})
		}
		result = append(result, FullAnalyticsType{
			ID:      oneType.ID,
			Name:    oneType.Name,
			Options: options,
		})
	}
	return result, nil
}

func (h *analyticsService) CreateAnalyticsType(ctx context.Context, newAnalytic AnalyticsType) error {
	return h.storage.CreateAnalyticsType(ctx, repo.AnalyticsType{Name: newAnalytic.Name})
}

func (h *analyticsService) GetAnalyticsType(ctx context.Context, id int64) (FullAnalyticsType, error) {
	typeDB, err := h.storage.GetAnalyticsType(
		ctx,
		id,
	)

	return fullAnalyticTypeFromDB(typeDB), err
}

func (h *analyticsService) UpdateAnalyticsType(ctx context.Context, analyticsType FullAnalyticsType) error {
	return h.storage.UpdateAnalyticsType(
		ctx,
		fullAnalyticTypeToDB(analyticsType),
	)
}

package service

import (
	tinkoffapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/client/tinkoff_api"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/common/optional"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

func apiInstrumentToDB(instrument tinkoffapi.Instrument) repo.Instrument {
	return repo.Instrument{
		ID:             instrument.ID,
		Tiker:          optional.From(instrument.Tiker),
		Name:           optional.From(instrument.Name),
		InstrumentType: optional.From(repo.InstrumentType(instrument.Type)),
		Currency:       optional.From(instrument.Currency),
		Lot:            optional.From(instrument.Lot),
	}
}

func convertAnalytics(in []repo.Analytic) []Analytic {
	result := make([]Analytic, 0, len(in))
	for _, analytic := range in {
		result = append(result, Analytic{
			ID:    analytic.ID,
			Owner: analytic.Owner,
			Name:  analytic.Name,
		})
	}
	return result
}

func fullAnalyticTypeToDB(in FullAnalyticsType) repo.FullAnalyticsType {
	result := repo.FullAnalyticsType{
		ID:      in.ID,
		Name:    in.Name,
		Options: make([]repo.FullAnalyticsTypeOptions, 0, len(in.Options)),
	}

	for _, option := range in.Options {
		result.Options = append(result.Options, repo.FullAnalyticsTypeOptions{
			ID:   option.ID,
			Name: option.Name,
		})
	}

	return result
}

func fullAnalyticTypeFromDB(in repo.FullAnalyticsType) FullAnalyticsType {
	result := FullAnalyticsType{
		ID:      in.ID,
		Name:    in.Name,
		Options: make([]FullAnalyticsTypeOptions, 0, len(in.Options)),
	}

	for _, option := range in.Options {
		result.Options = append(result.Options, FullAnalyticsTypeOptions{
			ID:   option.ID,
			Name: option.Name,
		})
	}

	return result
}

func TypeToBDO(in repo.InstrumentType) InstrumentType {
	switch in {
	case repo.InstrumentTypeBond:
		return InstrumentTypeBond
	case repo.InstrumentTypeCurrency:
		return InstrumentTypeCurrency
	case repo.InstrumentTypeEtf:
		return InstrumentTypeEtf
	case repo.InstrumentTypeShare:
		return InstrumentTypeShare
	}

	return InstrumentTypeUndefined
}

func TypeFromBDO(in InstrumentType) repo.InstrumentType {
	switch in {
	case InstrumentTypeBond:
		return repo.InstrumentTypeBond
	case InstrumentTypeCurrency:
		return repo.InstrumentTypeCurrency
	case InstrumentTypeEtf:
		return repo.InstrumentTypeEtf
	case InstrumentTypeShare:
		return repo.InstrumentTypeShare
	}

	return repo.InstrumentTypeUndefined
}

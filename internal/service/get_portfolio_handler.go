package service

import (
	"context"

	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
)

type PortfolioGetter interface {
	GetPortfolio(ctx context.Context) ([]Position, error)
}

type portfolioGetter struct {
	storage repo.Storage
}

func NewPortfolioGetter(storage repo.Storage) PortfolioGetter {
	return &portfolioGetter{
		storage: storage,
	}
}

func (h *portfolioGetter) GetPortfolio(ctx context.Context) ([]Position, error) {
	positions, err := h.storage.GetPortfolio(ctx)
	if err != nil {
		return nil, err
	}

	result := make([]Position, len(positions))
	for i, position := range positions {
		result[i] = Position{
			InstrumentID:   position.InstrumentID,
			CurrentSum:     position.CurrentSum,
			BuySum:         position.BuySum,
			Analytics:      convertAnalytics(position.Analytics),
			InstrumentType: TypeToBDO(position.InstrumentType),
		}
	}

	return result, nil
}

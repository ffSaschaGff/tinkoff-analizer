package service

type Instrument struct {
	ID             string         `json:"id"`
	Tiker          string         `json:"tiker"`
	Name           string         `json:"name"`
	Currency       string         `json:"currency"`
	Lot            int32          `json:"lot"`
	InstrumentType InstrumentType `json:"instrument_type"`
}

type Position struct {
	InstrumentID   string         `json:"instrument_id"`
	CurrentSum     float64        `json:"current_sum"`
	BuySum         float64        `json:"buy_sum"`
	Analytics      []Analytic     `json:"analytics"`
	InstrumentType InstrumentType `json:"instrument_type"`
}

type FullInstrument struct {
	ID             string                  `json:"id"`
	Tiker          string                  `json:"tiker"`
	Name           string                  `json:"name"`
	Currency       string                  `json:"currency"`
	Lot            int32                   `json:"lot"`
	InstrumentType InstrumentType          `json:"instrument_type"`
	Options        []FullInstrumentOptions `json:"options"`
}

type FullInstrumentOptions struct {
	Type  int64 `json:"type"`
	Value int64 `json:"value"`
}

type Analytic struct {
	ID    int64  `json:"id"`
	Owner int64  `json:"owner"`
	Name  string `json:"name"`
}

type AnalyticsType struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type FullAnalyticsType struct {
	ID      int64                      `json:"id"`
	Name    string                     `json:"name"`
	Options []FullAnalyticsTypeOptions `json:"options"`
}

type FullAnalyticsTypeOptions struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type InstrumentType string

const (
	InstrumentTypeUndefined InstrumentType = "INSTRUMENT_TYPE_UNDEFINED"
	InstrumentTypeShare     InstrumentType = "INSTRUMENT_TYPE_SHARE"
	InstrumentTypeBond      InstrumentType = "INSTRUMENT_TYPE_BOND"
	InstrumentTypeEtf       InstrumentType = "INSTRUMENT_TYPE_ETF"
	InstrumentTypeCurrency  InstrumentType = "INSTRUMENT_TYPE_CURRENSY"
)

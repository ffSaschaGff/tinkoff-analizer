package optional

type Optional[V any] struct {
	Value V
	Valid bool
}

func From[V any](value V) Optional[V] {
	return Optional[V]{
		Value: value,
		Valid: true,
	}
}

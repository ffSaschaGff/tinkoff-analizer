get_contracts:
	rm -Rf ./contracts
	mkdir ./contracts
	git clone https://github.com/Tinkoff/investAPI.git ./tmp_git
	sudo mv ./tmp_git/src/docs/contracts ./contracts/tinkoff_api
	rm -Rf ./tmp_git

generate:
	rm -Rf ./internal/pkg/tinkoff_api
	mkdir ./internal/pkg/tinkoff_api
	protoc --go_out=./internal/pkg/tinkoff_api --go_opt=paths=source_relative \
    --go-grpc_out=./internal/pkg/tinkoff_api --go-grpc_opt=paths=source_relative \
	--proto_path=./contracts/tinkoff_api \
    common.proto
	protoc --go_out=./internal/pkg/tinkoff_api --go_opt=paths=source_relative \
    --go-grpc_out=./internal/pkg/tinkoff_api --go-grpc_opt=paths=source_relative \
	--proto_path=./contracts/tinkoff_api \
	-I=./contracts/tinkoff_api/common.proto \
    operations.proto
	protoc --go_out=./internal/pkg/tinkoff_api --go_opt=paths=source_relative \
    --go-grpc_out=./internal/pkg/tinkoff_api --go-grpc_opt=paths=source_relative \
	--proto_path=./contracts/tinkoff_api \
	-I=./contracts/tinkoff_api/common.proto \
    users.proto
	protoc --go_out=./internal/pkg/tinkoff_api --go_opt=paths=source_relative \
    --go-grpc_out=./internal/pkg/tinkoff_api --go-grpc_opt=paths=source_relative \
	--proto_path=./contracts/tinkoff_api \
	-I=./contracts/tinkoff_api/common.proto \
    instruments.proto
	protoc --go_out=./internal/pkg/tinkoff_api --go_opt=paths=source_relative \
    --go-grpc_out=./internal/pkg/tinkoff_api --go-grpc_opt=paths=source_relative \
	--proto_path=./contracts/tinkoff_api \
	-I=./contracts/tinkoff_api/common.proto \
    marketdata.proto

migrate:
	go install github.com/pressly/goose/v3/cmd/goose@latest
	goose -dir=migrations/postgres postgres "host=$(DB_HOST) user=$(DB_LOGIN) password=$(DB_PASSWORD) dbname=$(DB_NAME) sslmode=disable" up

lint:
	golangci-lint run

test:
	go test -v -timeout 120s -count=1 ./... -coverprofile=coverage.txt -covermode count
	go get github.com/boumenot/gocover-cobertura
	go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
	go install gotest.tools/gotestsum@latest
	gotestsum --junitfile report.xml --format testname

mockgen:
	mockgen -source=internal/client/tinkoff_api/api.go -destination=internal/client/tinkoff_api/mock/mock.go -package=mock Client
	mockgen -source=internal/repo/datastruct.go -destination=internal/repo/mock/mock.go -package=mock Storage

let chart
let app

document.addEventListener("DOMContentLoaded", function(){
    queryString = location.search
    params = new URLSearchParams(queryString)

    app = new Vue({
        el: '#app',
        data:{
            instrument: {
                id: params.get("id"),
                name: "",
                tiker: "",
                options: [],
            },
            analytics: [],
            errorsList: [],
        },
        methods: {
            save: save,
        },
    })
    
    init();
    checkErrors();
})

function save() {
    options = []
    for (option of app.analytics) {
        if (!option.selected) {
            continue;
        }
        options.push({
            "type":option.id,
            "value":option.selected,
        });
    }
    askAPI("/api_v1/instruments/" + app.instrument.id + "/options", "PUT", JSON.stringify(options))
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
        
            init();
        })
}

function init() {
    askAPI("/api_v1/instruments/" + app.instrument.id, "GET")
    .then(rawResult => {
        if (!rawResult.ok) {
            addMessage(rawResult.result);
            return;
        }
    
        app.instrument.name = rawResult.result.name;
        app.instrument.tiker = rawResult.result.tiker;
        app.instrument.options = rawResult.result.options;

        askAPI("/api_v1/analytics_type", "GET")
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
        
            for (analytic of rawResult.result) {
                analytic.selected = 0;
            }
            app.analytics = rawResult.result;

            optionMap = new Map();
            for (option of app.instrument.options) {
                optionMap.set(option.type, option.value);
            }
            for (analytic of app.analytics) {
                if (!optionMap.has(analytic.id) || analytic.options == null) {
                    continue;
                }

                for (analyticValue of analytic.options) {
                    if (optionMap.get(analytic.id) == analyticValue.id) {
                        analytic.selected = analyticValue.id;
                    }
                }
            }
        })
    })
}

async function askAPI(path, method, params) {
      req = {
        method: method,
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        }
      }
      paramsPath = path;
      if (method == "GET") {
        if (params) {
            paramsPath = paramsPath + "?" + new URLSearchParams(params);
        }
      } else {
        if (params) {
            req.body = params;
          }
      }

      let response = await fetch(".." + paramsPath, req);
      
      let result = await response.json();
      return {
        result: result,
        ok: response.ok
      };
}

function truncateNumber(source) {
    return Math.round(source * 100) / 100;
}

function formatMoney(source) {
    const formatter = new Intl.NumberFormat('ru-RU', {
        style: 'currency',
        currency: 'RUB',
      });
    return formatter.format(source)
}

function checkErrors() {
  errorLifetime = 10000;
  setInterval(function() {
      now = new Date();
      nowMs = now.getTime();
      newErrors = [];
      for (error of app.errorsList) {
          if (error.time + errorLifetime > nowMs) {
              newErrors.push(error)
          }
      }
      app.errorsList = newErrors
  }, 1000)
}

function addMessage(event) {
  if (!event.msg) {
    return;
  }

  now = new Date()
  app.errorsList.push({
      msg: event.msg,
      time: now.getTime(),
  });
}

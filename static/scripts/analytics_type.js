let app

document.addEventListener("DOMContentLoaded", function(){
    queryString = location.search
    params = new URLSearchParams(queryString)
    id = parseInt(params.get("id"))

    app = new Vue({
        el: '#app',
        data: {
            analyticsType: {
                id: id,
                name: "",
                options: [],
            },
            errorsList: [],
        },
        methods: {
            addRow: function() {app.analyticsType.options.push({name:"",id:0})},
            save: save,
        },
    })
    
    init();
    checkErrors();
})

function save() {
    askAPI("/api_v1/analytics_type", "PUT", JSON.stringify(app.analyticsType))
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
        
            init();
        })
}

function init() {
    askAPI("/api_v1/analytics_type/" + app.analyticsType.id, "GET",)
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
        
            app.analyticsType = rawResult.result;
        })
}

let chart
let app

document.addEventListener("DOMContentLoaded", function(){
    app = new Vue({
        el: '#app',
        data: {
          instruments: [],
          groupOptions: [],
          groupSelected: 0,
          modalIsVisible: false,
          newAnalyticsType: {
            name: "",
          },
          errorsList: [],
        },
        methods: {
            saveAnalytics: saveAnalytics,
            changeGroup: loadChart,
            loadInstruments: refreshPortfolio,
            openAnalytiics: openAnalytiics,
        },
    })
    
    init();
    loadChart();
    checkErrors();
})

function openAnalytiics() {
    if (app.groupSelected == 0) {
        return;
    }

    location = "analytics_type.html?id=" + app.groupSelected;
}

// Модальное окно аналитики
function createAtalyticsType() {
    modal = document.getElementById("modal")
    modal.style.display = "block";
}

function modalClose() {
    modal = document.getElementById("modal")
    modal.style.display = "";
}

function saveAnalytics() {
    analiticsTypeName = app.newAnalyticsType.name
    if (!analiticsTypeName) {
        alert("Имя не может быть пустым!")
    }

    askAPI("/api_v1/analytics_type", "POST", {name: analiticsTypeName})
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
            modalClose();
        });
}

// Обновление портфеля
function refreshPortfolio() {
    askAPI("/api_v1/load_portfolio", "POST")
        .then(rawResult => {
            if (!rawResult.ok) {
                addMessage(rawResult.result);
                return;
            }
            loadChart();
        });
}

function init() {
    loadGroups()
}

function loadGroups() {
    askAPI("/api_v1/analytics_type", "GET")
        .then(typesRes => {
            if (!typesRes.ok) {
                addMessage(typesRes.result);
                return;
            }
            types = typesRes.result;
            app.groupOptions = [];
            app.groupOptions.push({id: 0, name: "Инструменты"});
            for (let type of types) {
                app.groupOptions.push({id: type.id, name: type.name})
            }
        });
}

async function getPortfolio() {
    groupBy = app.groupSelected;
    res = await askAPI("/api_v1/portfolio", "GET");
    if (!res.ok) {
        addMessage(res.result);
        return;
    }
    rawResult = res.result;
    portfolioMap = new Map();
    ids = [];
    for (i in rawResult) {
        portfolioMap.set(rawResult[i].instrument_id, rawResult[i]);
        ids.push(rawResult[i].instrument_id);
    }

    res = await askAPI("/api_v1/instruments", "GET", {IDs: ids});
    if (!res.ok) {
        addMessage(res.result);
        return;
    }
    rawInstruments = res.result;
    for (i in rawInstruments) {
        if (portfolioMap.has(rawInstruments[i].id)) {
            position = portfolioMap.get(rawInstruments[i].id);
            position.tiker = rawInstruments[i].tiker;
            position.name = rawInstruments[i].name;
            position.id = rawInstruments[i].id;
            portfolioMap.set(rawInstruments[i].id, position);
        }
    }

    resultByInstrument = [];
    for (let position of portfolioMap.values()) {
        resultByInstrument.push(
            {
                name: position.tiker,
                description: position.name,
                id: position.id,
                sum: truncateNumber(position.current_sum)
            }
        );
    }
    drawInstrumentsTable(resultByInstrument);
    result = [];

    if (groupBy == 0) {
        result = resultByInstrument
    } else {
        groupMap = new Map();
        groupMap.set(0, {id: 0, name: "Не указано", sum: 0});

        for (let position of portfolioMap.values()) {
            id = 0;
            group = groupMap.get(0);
            for (let analytic of position.analytics) {
                if (analytic.owner == groupBy) {
                    id = analytic.id;
                    if (groupMap.has(id)) {
                        group = groupMap.get(id);
                        group.sum += position.current_sum;
                    } else {
                        group = {id: id, name: analytic.name, sum: position.current_sum};
                    }
                    break;
                }
            }
            if (id == 0) {
                group.sum += position.current_sum;
            }
            groupMap.set(id, group);
        }

        for (let position of groupMap.values()) {
            result.push(
                {
                    name: position.name,
                    sum: truncateNumber(position.sum)
                }
            );
        }
    }

    return result;
}

function drawInstrumentsTable(instruments) {
    instruments.sort(chartSorter);

    app.instruments = [];
    for (let position of instruments) {
        app.instruments.push(
            {
                tiker: position.name,
                name: position.description,
                id: position.id,
                sum: formatMoney(position.sum),
            }
        );
    }
}

function loadChart() {
    getPortfolio()
    .then(portfolio => {
        if (chart) {
            chart.destroy();
        }
        chartData = {
            labels: [],
            datasets: [
                {
                    data: [],
                    borderWidth: 1
                }
            ]
        }

        portfolio.sort(chartSorter);
        sum = 0;
        for (i in portfolio) {
            chartData.labels.push(portfolio[i].name);
            chartData.datasets[0].data.push(portfolio[i].sum);
            sum += portfolio[i].sum;
        }
        document.getElementById("portfolio_chart_caption").textContent = formatMoney(sum);
    
        const ctx = document.getElementById('portfolio_chart');
    
        chart = new Chart(ctx, {
          type: 'pie',
          data: chartData,
          options: {
            plugins: {
                legend: {display: false},
            },
          },
        });
    });

}

function chartSorter(a, b) {
    return b.sum - a.sum;
}

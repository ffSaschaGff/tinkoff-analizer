package main

import (
	"log"
	"os/exec"
	"runtime"
	"time"

	tinkoffapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/client/tinkoff_api"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/config"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo/postgres"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/repo/sqlite"
	selfapi "gitlab.com/ffsaschagff/tinkoff-analizer/internal/self_api"
	"gitlab.com/ffsaschagff/tinkoff-analizer/internal/service"
)

func main() {
	log.SetFlags(log.Lshortfile)
	var (
		storage repo.Storage
		err     error
	)

	dbType := config.GetValue(config.DatabaseType)
	switch dbType {
	case repo.DBTypePostgres:
		storage, err = postgres.NewStorage()
	case repo.DBTypeSQLite:
		storage, err = sqlite.NewStorage()
	default:
		log.Fatalf("invalid DB type %s", dbType)
	}

	if err != nil {
		log.Fatal(err)
	}

	api, err := tinkoffapi.NewClient(string(config.GetValue(config.TinkoffToken)))
	if err != nil {
		log.Fatal(err)
	}

	ourAPI := selfapi.NewSelfAPI(
		service.NewPortfolioGetter(storage),
		service.NewInstrumentsLoader(storage, api),
		service.NewInstrumentService(storage),
		service.NewAnalyticsService(storage),
	)

	go func() {
		time.Sleep(time.Second)
		if config.GetValue(config.OpenOnStart).Bool() {
			err = open("http://localhost:" + config.GetValue(config.HTTPPort).String() + "/static/portfolio.html")
			if err != nil {
				log.Println(err)
			}
		}
	}()

	ourAPI.Run()
}

func open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default:
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

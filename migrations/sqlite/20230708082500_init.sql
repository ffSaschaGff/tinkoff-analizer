-- +goose Up 
CREATE TABLE IF NOT EXISTS instrument(
    id TEXT PRIMARY KEY,
    tiker TEXT,
    name TEXT,
    currency TEXT,
    type TEXT,
    lot INTEGER
    );
CREATE TABLE IF NOT EXISTS account(
    id TEXT PRIMARY KEY, 
    name TEXT
    );
CREATE TABLE IF NOT EXISTS portfolio(
    instrument_id TEXT,
    account_id TEXT,
    current_price REAL,
    average_price REAL,
    quantity REAL,
    PRIMARY KEY (instrument_id, account_id)
    );
CREATE TABLE IF NOT EXISTS currency(
    id TEXT PRIMARY KEY, 
    instrument_id TEXT,
    price REAL
    );
CREATE TABLE IF NOT EXISTS analytic_types(
    id INTEGER PRIMARY KEY, 
    name TEXT
    );
CREATE TABLE IF NOT EXISTS analytics(
    id INTEGER PRIMARY KEY, 
    type_id INTEGER,
    name TEXT
    );
CREATE TABLE IF NOT EXISTS analytic_value(
    analytic_types INTEGER, 
    instrument_id TEXT,
    analytic_id INTEGER,
    PRIMARY KEY (analytic_types, instrument_id)
    );
INSERT INTO currency (id, instrument_id)
    VALUES
    ('usd', 'a22a1263-8e1b-4546-a1aa-416463f104d3'),
    ('jpy', '163ce2bf-37a1-42e0-b613-0e50a15f8933'),
    ('chf', 'b8ef7e05-97ef-4b78-aac3-cd16ee320168'),
    ('rub', 'a92e2e25-a698-45cc-a781-167cf465257c'),
    ('gbp', 'dfcccc14-b90d-476f-82e3-ce55c8093afc'),
    ('eur', 'b1c06e5e-f5d4-4ff5-8cb3-dcacccd933da'),
    ('cny', '4587ab1d-a9c9-4910-a0d6-86c7b9c42510'),
    ('kzt', 'c7c26356-7352-4c37-8316-b1d93b18e16e'),
    ('hkd', 'c1bcc46b-32c1-47dc-93b4-1db269c103a4')
    ON CONFLICT (id) DO UPDATE 
    SET instrument_id = excluded.instrument_id;
-- +goose Down
DROP TABLE IF EXISTS instrument;
DROP TYPE IF EXISTS instrument_type;
DROP TYPE IF EXISTS portfolio;
DROP TYPE IF EXISTS account;
DROP TYPE IF EXISTS currency;
DROP TABLE IF EXISTS analytic_value;
DROP TABLE IF EXISTS analytics;
DROP TABLE IF EXISTS analytic_types;

-- +goose Up 
CREATE TABLE IF NOT EXISTS currency(
    id TEXT PRIMARY KEY, 
    instrument_id TEXT,
    price DOUBLE PRECISION
    );

-- +goose Down
DROP TYPE IF EXISTS currency;

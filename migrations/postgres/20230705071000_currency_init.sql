-- +goose Up
INSERT INTO currency (id, instrument_id)
VALUES
('usd', 'a22a1263-8e1b-4546-a1aa-416463f104d3'),
('jpy', '163ce2bf-37a1-42e0-b613-0e50a15f8933'),
('chf', 'b8ef7e05-97ef-4b78-aac3-cd16ee320168'),
('rub', 'a92e2e25-a698-45cc-a781-167cf465257c'),
('gbp', 'dfcccc14-b90d-476f-82e3-ce55c8093afc'),
('eur', 'b1c06e5e-f5d4-4ff5-8cb3-dcacccd933da'),
('cny', '4587ab1d-a9c9-4910-a0d6-86c7b9c42510'),
('kzt', 'c7c26356-7352-4c37-8316-b1d93b18e16e'),
('hkd', 'c1bcc46b-32c1-47dc-93b4-1db269c103a4')
ON CONFLICT (id) DO UPDATE 
  SET instrument_id = excluded.instrument_id;

-- +goose Up 
CREATE TYPE instrument_type AS ENUM ('shares', 'currency', 'bond', 'etf');

CREATE TABLE IF NOT EXISTS country(
    id INTEGER PRIMARY KEY,
    name TEXT
    );

CREATE TABLE IF NOT EXISTS industry(
    id INTEGER PRIMARY KEY,
    name TEXT
    );

CREATE TABLE IF NOT EXISTS instrument(
    id TEXT PRIMARY KEY,
    tiker TEXT,
    name TEXT,
    currency TEXT,
    type instrument_type,
    industry INTEGER,
    country INTEGER,
    lot INTEGER
    );
-- +goose Down
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS industry;
DROP TYPE IF EXISTS instrument_type;

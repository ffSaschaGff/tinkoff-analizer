-- +goose Up
CREATE SEQUENCE analytic_types_sequence
    INCREMENT 1
    START 1
    MINVALUE 1
;
CREATE SEQUENCE analytics_sequence
    INCREMENT 1
    START 1
    MINVALUE 1
;
ALTER TABLE analytic_types ALTER COLUMN id SET DEFAULT nextval('analytic_types_sequence'::regclass);
ALTER TABLE analytics ALTER COLUMN id SET DEFAULT nextval('analytics_sequence'::regclass);

-- +goose Down
ALTER TABLE analytic_types ALTER COLUMN id DROP DEFAULT;
ALTER TABLE analytics ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE analytic_types_sequence;
DROP SEQUENCE analytics_sequence;
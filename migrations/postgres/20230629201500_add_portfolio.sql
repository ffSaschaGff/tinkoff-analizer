-- +goose Up 
CREATE TABLE IF NOT EXISTS account(
    id TEXT PRIMARY KEY, 
    name TEXT
    );

CREATE TABLE IF NOT EXISTS portfolio(
    instrument_id TEXT,
    account_id TEXT,
    current_price REAL,
    average_price REAL,
    quantity DOUBLE PRECISION,
    PRIMARY KEY (instrument_id, account_id)
    );
-- +goose Down
DROP TYPE IF EXISTS portfolio;
DROP TYPE IF EXISTS account;

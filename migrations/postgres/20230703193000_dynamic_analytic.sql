-- +goose Up 
CREATE TABLE IF NOT EXISTS analytic_types(
    id INTEGER PRIMARY KEY, 
    name TEXT
    );
CREATE TABLE IF NOT EXISTS analytics(
    id INTEGER PRIMARY KEY, 
    type_id INTEGER,
    name TEXT
    );
CREATE TABLE IF NOT EXISTS analytic_value(
    analytic_types INTEGER, 
    instrument_id TEXT,
    analytic_id INTEGER,
    PRIMARY KEY (analytic_types, instrument_id)
    );
ALTER TABLE instrument
    DROP COLUMN industry,
    DROP COLUMN country;
DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS industry;

-- +goose Down
DROP TABLE IF EXISTS analytic_value;
DROP TABLE IF EXISTS analytics;
DROP TABLE IF EXISTS analytic_types;
ALTER TABLE instrument
    ADD COLUMN industry INTEGER,
    ADD COLUMN country INTEGER;
CREATE TABLE IF NOT EXISTS country(
    id INTEGER PRIMARY KEY,
    name TEXT
    );
CREATE TABLE IF NOT EXISTS industry(
    id INTEGER PRIMARY KEY,
    name TEXT
    );
